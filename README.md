# 300 FG Tanzania microbiome, metabolome and immunity analysis documentation

This repository contains the code to reproduce the analysis in the study investigating the effects of gut microbiota on metabolism and immunity in a cohort of rural and
urban dwellers from Tanzania, Africa. The results are presented in the paper

*Gut microbiome-mediated metabolism effects on immunity in rural and urban African populations*

by

Martin Stražar, Godfrey S. Temba, Hera Vlamakis, Vesla I. Kullaya, Furaha Lyamuya, 
Blandina T. Mmbaga, Leo A. B. Joosten, Andre J. A. M. van der Ven, Mihai G. Netea, 
Quirijn de Mast, and Ramnik J. Xavier

(2021, submitted)

## Data storage

All data used in the study is stored in the R data object `data.Robj`. This object is loaded in every script as variable `obj`. 

The object contains the following data objects called assays. Each assay consist of two entries `data` (numerical data matrix of samples and features) and `metadata` (additional information on features).

For example, abundance of 212 microbial species is stored as in `obj$Species`,  while their names are stored in   `obj$Species$metadata`. 

General:

* `obj$metadata` Meta data on study participants (age, sex, previous vaccinations, ...).
* `obj$assays$Diet`	 matrix size 323 x 17.

Metagenomics:

* `obj$assays$Kingdom`	 matrix size 315 x 3.  Relative abundance of microbes on kingdom level (Metaphlan 2.2).
* `obj$assays$Phylum`	 matrix size 315 x 9.   Relative abundance of microbes on phylum level (Metaphlan 2.2).
* `obj$assays$Class`	 matrix size 315 x 14.  Relative abundance of microbes on class level (Metaphlan 2.2).
* `obj$assays$Order`	 matrix size 315 x 20.  Relative abundance of microbes on order level (Metaphlan 2.2).
* `obj$assays$Family`	 matrix size 315 x 40.  Relative abundance of microbes on family level (Metaphlan 2.2).
* `obj$assays$Genus`	 matrix size 315 x 82.  Relative abundance of microbes on genus level (Metaphlan 2.2).
* `obj$assays$Species`	 matrix size 315 x 212. Relative abundance of microbes on species level (Metaphlan 2.2).
* `obj$assays$Species_from_lm`	 matrix size 315 x 34. Relative abundance of 43 immunomodulatory microbial species.
* `obj$assays$Eukaryota`	 matrix size 320 x 5. Relative abundance of 5 detected eukaryotes on species or genus level (Metaphlan 2.2).
* `obj$assays$ko`	 matrix size 316 x 138092. Abundance of genes annotated by KEGG Orthogroup (KO, humann2).
* `obj$assays$ec`	 matrix size 316 x 83855. Abundance of genes annotated by Enzyme commision numbers (EC, humann2).
* `obj$assays$pathways`	 matrix size 316 x 9293. Pathway copy numbers for each microbe (humann2).
* `obj$assays$PathwaysMetaCyc`	 matrix size 2074 x 2. List of MetaCyc pathway IDs and names.
* `obj$assays$Pathways_enzymes_KO`	 matrix size 25219 x 4. Enzymes in pathways marked annotated by KEGG Orthogroup (KO).

Metabolomics:

* `obj$assays$Metabolites`	 matrix size 323 x 1607. Intensity of 1,607 untargeted metabolite features.
* `obj$assays$Pathways_metabolites_KO`	 matrix size 2385 x 4. Compounds in pathways marked annotated by KEGG Orthogroup (KO).

Cytokine production assays (ELISA):

* `obj$assays$Cytokines`	 matrix size 307 x 47. Expression of TNF-α, IFN-γ, IL-1β, IL-6 and IL-10 upon
ex vivo stimulation with   LPS, Poly:IC, <i>C. burnetii</i>, <i>E. coli</i> <i>S. typhi</i> <i>S. enteritidis</i>
<i>S. aureus</i> <i>S. pneumoniae</i> <i>M. tuberculosis</i> and <i>C. albicans</i>.


## Analysis scripts

The scripts to generate shall be run from the project source directory.
Details about the used methods can be found in code and the paper. Completed list of required R 
dependencies is listed at the end of this document.

### Basic cohort statistics and microbiome composition 

All the data used in the scripts is stored in the common R object.

    DATA=./data/data.TZ.Robj
        
Basic variable distribution.

    OUT=./output/composition/
    ./code/composition/variable_distribution.R $DATA $OUT

Plot abundance of phyla and fungi. Compare with 500 FG (Netherlands) cohort.

    OUT=./output/composition/
    DATA_NL=./data/data.NL.Robj

    ./code/composition/abundance_phyla.R $DATA $OUT
    ./code/composition/abundance_phyla_fg500.R $DATA_NL $OUT
    ./code/composition/abundance_fungi.R $DATA $OUT

Compare compositions with available cohorts with 500FG. Compute species differential abundance (Maaslin2) and species changing along 
the urbanization gradient. Comparison on other taxonomic levels is possible.
    
    OUT=./output/cohorts/   
    DATA_NL=./data/data.NL.Robj

    ./code/cohorts/cohorts_maaslin.R $DATA $DATA_NL $OUT Species
    ./code/cohorts/cohorts_gradient.R $DATA $DATA_NL $OUT Species

Compare strain variation using pre-compute StrainPhlan trees.
    
    TREES=./data/strainphlanTree/
    OUT=./output/strains
    ./code/strains/strains_trees.R $DATA $TREES $OUT

Compare functional differences with 500FG using Humann2 pathways.

    DATA=./data/data.TZ.Robj
    DATA_NL=./data/data.NL.Robj
    OUT=./output/cohorts/pathways/
    ./code/cohorts/cohorts_maaslin_pathways.R $DATA $DATA_NL $OUT

  
### Factors driving microbial profiles

Cluster microbiome compositions, plot projections and optionally store cluster indicators.

    OUT=./output/clusters
    SAVE=FALSE
    ./code/clusters/clustering.R $DATA $OUT $SAVE
    ./code/clusters/cluster_assoc.R $DATA $OUT
    
Phylum effect on metadata variables. 

    OUT=./output/clusters/maaslin_phylum/
    ./code/clusters/variable_phylum.R $DATA $OUT

Dietary effects on categorical variables.

    OUT=./output/clusters/diet/
    ./code/clusters/variable_diet.R $DATA $OUT


### Immune responses and immunomodulatory microbe

Basic response statistics for urban/rural populations.

    OUT=./output/response/
    ./code/response/response_stats.R $DATA $OUT
  
Linear model to find immunomodulatory species. 
Optionally, save species to common data object as `Species_from_lm` for downstream analysis.

    SAVE=FALSE    
    ./code/response/lm_species.R $DATA $OUT $SAVE
  
Compare species effects to those computed in 500 FG (Netherlands) cohort using the same method.
  
    OUT=./output/response/500fg/
    TZR="./output/response/results.csv"
    DATA_NL=./data/data.NL.Robj
    ./code/response/lm_species_500fg.R $DATA $TZR $DATA_NL $OUT
    
Compare results with linear mixed model with Sample ID as a random effect.
    
    OUT=./output/response/mixed/
    ./code/response/lm_species_mixed.R $DATA $OUT
    


### Metabolomics

Analyses using metabolomics data and immune responses alone.

Perform metabolomics quality control via clustering.
Optionally, save metabolomics clusters for downstream analysis. 

    OUT=./output/metabolomics/
    SAVE=FALSE
    ./code/metabolomics/cluster_mbx.R $DATA $OUT $SAVE

Differential analysis between rural and urban samples.
    
    OUT=./output/metabolomics/differential
    ./code/metabolomics/differential_mbx_residency.R $DATA $OUT

Correlations between metabolites and cytokines subject to adjusted p-value `FDR`.
    
    OUT=./output/metabolomics/correlations_cytokines/
    FDR=.05
    ./code/metabolomics/correlations_pairwise.R $DATA $OUT Metabolites Cytokines $FDR
    
Display full metabolite cytokine correlation matrix. 
Input correlation results from previous step. 
Display pathways with at least `N` metabolites. 

    OUT=./output/metabolomics/correlations_cytokines/
    CORR=./output/metabolomics/correlations_cytokines/results.csv.gz
    N=15
    ./code/metabolomics/pathway_metabolites.R $DATA $CORR $OUT $FDR $N

Aggregate cytokine correlations per pathway. Count negative and positive correlations
separately. Keep pathways with at least `M` compounds.  

    M=3
    ./code/metabolomics/pathway_enrichment.R $DATA $CORR $OUT $FDR $M
   
    
        
### Microbiome effect on the metabolome

Compute correlations between microbes and metabolites. Link from metabolomics.

    OUT=./output/metabolomics/correlations_species/
    FDR=.05
    ./code/metabolomics/correlations_pairwise.R $DATA $OUT Metabolites Species $FDR

Correlations between metabolites and species selected based on the linear model above.

    OUT=./output/metabolomics/correlations_species/
    CORR=./output/metabolomics/correlations_species/results.csv.gz
    FDR=0.05
    ./code/metabolomics/correlations_species_metabolites.R $DATA $CORR $OUT $FDR
    ./code/metabolomics/correlations_species_metacyc.R $DATA $CORR $OUT $FDR
    
Compute pathway copy number enrichment on immunomodulatory species using the MetaCyc annotation.
Use nominal p-value cutoff `ALPHA`.

    OUT=./output/metabolomics/metacyc/
    ALPHA=0.05
    ./code/metabolomics/pathway_enrichment_metacyc.R $DATA $OUT $ALPHA
    
Integrate metabolomics and metagenomics enrichment for immunomodulatory species.

     PTH="./output/metabolomics/metacyc/summary.csv"
     MBX="./output/metabolomics/correlations_species/SPP_effects_metacyc.csv"
     MBD="./output/metabolomics/correlations_species/SPP_effects_metacyc_details.csv"
     OUT="./output/metabolomics/integrate/"
     ./code/metabolomics/integrate_metacyc.R $PTH $MBX $MBD $OUT
     

Draw pathway graphs, detected enzymes, compounds and their effects on cytokines.

    ko00220: Arginine biosynthesis	
    ko00340: Histidine metabolism
    
    CORR=./output/metabolomics/correlations_cytokines/results.csv.gz
    OUT="./output/metabolomics/graphs/"
    KGML="./data/kgml/"
    ./code/metabolomics/pathway_graph.R $DATA $KGML $CORR $OUT ko00220
    ./code/metabolomics/pathway_graph.R $DATA $KGML $CORR $OUT ko00340 


## Dependencies

The scripts depend on one or more the following R packages listed in 

    ./requirements.R
    
Dependencies installation is done as in

    ./install.R 
    
and should take minutes. 

The versions were as follows:
    
    > sessionInfo()
    R version 3.6.1 (2019-07-05)
    Platform: x86_64-apple-darwin15.6.0 (64-bit)
    Running under: macOS Catalina 10.15.7
    
    Matrix products: default
    BLAS:   /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRblas.0.dylib
    LAPACK: /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRlapack.dylib
    
    locale:
    [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
    
    attached base packages:
    [1] grid      splines   stats     graphics  grDevices utils     datasets 
    [8] methods   base     
    
    other attached packages:
     [1] vegan_2.5-7       lattice_0.20-38   permute_0.9-5     stringr_1.4.0    
     [5] scales_1.0.0      reshape2_1.4.3    pvclust_2.2-0     philentropy_0.4.0
     [9] pheatmap_1.0.12   mltools_0.3.5     mclust_5.4.5      igraph_1.2.4.1   
    [13] ggtree_2.0.4      ggrepel_0.8.2     ggpubr_0.2.4      magrittr_1.5     
    [17] fmsb_0.7.0        entropy_1.2.1     egg_0.4.5         ggplot2_3.3.0    
    [21] gridExtra_2.3     data.table_1.12.8 corrplot_0.84     colorspace_1.4-1 
    [25] ape_5.4-1         Rtsne_0.15        Maaslin2_1.0.0    CCA_1.2          
    [29] fields_10.0       maps_3.3.0        spam_2.4-0        dotCall64_1.0-0  
    [33] fda_2.4.8         Matrix_1.2-17    
    
    loaded via a namespace (and not attached):
     [1] Rcpp_1.0.3          mvtnorm_1.0-11      tidyr_1.0.0        
     [4] R6_2.4.1            plyr_1.8.4          pcaPP_1.9-73       
     [7] pillar_1.4.2        rlang_0.4.10        lazyeval_0.2.2     
    [10] munsell_0.5.0       compiler_3.6.1      pkgconfig_2.0.3    
    [13] biglm_0.9-1         mgcv_1.8-28         tidyselect_1.1.0   
    [16] tibble_2.1.3        crayon_1.3.4        dplyr_1.0.5        
    [19] withr_2.1.2         MASS_7.3-51.4       nlme_3.1-140       
    [22] jsonlite_1.6        gtable_0.3.0        lifecycle_1.0.0    
    [25] DBI_1.1.0           stringi_1.4.3       tidytree_0.3.3     
    [28] ggsignif_0.6.0      getopt_1.20.3       lpsymphony_1.14.0  
    [31] robustbase_0.93-5   optparse_1.6.4      rvcheck_0.1.8      
    [34] generics_0.0.2      vctrs_0.3.7         RColorBrewer_1.1-2 
    [37] tools_3.6.1         treeio_1.10.0       glue_1.4.2         
    [40] DEoptimR_1.0-8      purrr_0.3.3         parallel_3.6.1     
    [43] cluster_2.1.0       BiocManager_1.30.10