#!/usr/bin/env Rscript

# Installation through CRAN
install.packages(c("CCA",  "Matrix",  "Rtsne", "ape", "colorspace",  "corrplot", "data.table",
    "egg", "entropy", "fmsb", "ggplot2", "ggpubr", "ggrepel", "ggtree", "igraph", "mclust", "mltools", "pheatmap",
    "philentropy", "pvclust", "reshape2", "scales", "stringr"))

# Installation throuh BiocManager
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install("Maaslin2")
