#!/usr/bin/env Rscript

hlp = " Visualization of cohorts on a common plane. "

# Read pipeline arguments
args = commandArgs(trailingOnly = TRUE)
if(length(args) < 2){
  message(hlp)
  message(sprintf("Wrong input"))
  message(sprintf("Usage: ./cohorts_maaslin.R in.tz in.nl out assay "))
  q(1, save = "no")
}

# Paths
in.tz = args[1]
in.nl = args[2]
out.dir = args[3]
assay = args[4]

# Libs 
require(philentropy)
require(ggplot2)
require(Rtsne)
require(Maaslin2)
require(scales)
require(ggpubr)
require(reshape2)
require(vegan)

# Figure parameters
RURAL = "#db7b14"
URBAN = "#0dafdb"

# Approximate color scale
LOW = "#132C45"
MID = "#356E9D"
HIGH ="#55AEF3"

# Paths 
dir.create(out.dir, recursive = TRUE, showWarnings = FALSE)

# List available cohorts
inputs = list(fg500=in.nl, 
              fg300tanzania=in.tz)


# Design ; Age and Gender not available for FG500
fixed_effects = c("Cohort")
random_effects = c()

# Load data on cohorts
M = NULL
meta = NULL
for(coh in names(inputs)){
  in.data = inputs[[coh]]
  load(in.data)
  obj = .GlobalEnv$obj
  Sx = obj$assays[[assay]]$data
  Sx = Sx / rowSums(Sx)
  df = data.frame(row.names = row.names(Sx), 
                  Sample = row.names(Sx),
                  Cohort = coh,
                  Gender = obj$metadata[row.names(Sx), "Gender"],
                  Age = obj$metadata[row.names(Sx), "Age"],
                  Reads = obj$metadata[row.names(Sx), "Reads"],
                  Markers = obj$metadata[row.names(Sx), "Markers"],
                  Genes = obj$metadata[row.names(Sx), "Genes"])
  inxs = is.na(df$Age)
  df$Age[inxs] = median(df$Age, na.rm = T)
  if(is.null(M)){
    M = Sx
    meta = df
  } else {
    keep = intersect(colnames(M), colnames(Sx))
    M = rbind(M[,keep], Sx[,keep])
    meta = rbind(meta, df)
  }
  message(sprintf("Adding cohort %s, keeping %d features in %s",
                  coh, ncol(M), assay))
  message(sprintf("Imputed missing age for %d samples", sum(inxs)))
}

# Re-normalize to kepth species
message(sprintf("Renormalizing %d features", ncol(M)))
M = M / rowSums(M)

# Add label
tmf = obj$metadata
meta$Cluster = as.character(tmf[row.names(meta), "Cluster"])
meta[is.na(meta$Cluster), "Cluster"] = ""
meta$Residency = as.character(tmf[row.names(meta), "Residency_Area"])
levels(meta$Cohort) = c("NL ", "TZ")
meta$Label = as.character(meta$Cohort)
inxs = meta$Cohort == "TZ"
meta[inxs,]$Label = sprintf("%s %s", meta[inxs,]$Cohort, meta[inxs,]$Residency)

# Log10Markers
meta$Log10Markers = cut(log10(meta$Markers), breaks = 3)


# Compute distance between rows
D = JSD(M)
mds <- cmdscale(D, eig=FALSE, k=2)
meta$MDS1 = mds[,1]
meta$MDS2 = mds[,2]

# Compute t-SNE
set.seed(46)
tsne = Rtsne(D)
meta$TSNE1 = tsne$Y[,1]
meta$TSNE2 = tsne$Y[,2]

# Run Maaslin2 for selected assay
maas.dir = file.path(out.dir, "maaslin")
maas = Maaslin2(input_data = t(M),
                transform = "LOG",
                input_metadata = meta[,c(fixed_effects, random_effects), drop=FALSE],
                output = maas.dir,
                random_effects = random_effects,
                fixed_effects = fixed_effects)
res = maas$results

# Compute PERMANOVA
ad = adonis(as.dist(D) ~ Age + Gender + Label, data=meta)
pmf = data.frame(ad$aov.tab)
fname = file.path(out.dir, "permanova.csv")
write.csv(pmf, fname, row.names = T)
message(sprintf("Written %s", fname))
pff = pmf[1:3,]
fname = file.path(out.dir, "permanova.pdf")
ggplot(data=pff, mapping = aes(y=gsub("Label", "NL / TZ U. / TZ R.", 
                                 row.names(pff)), x=100*R2)) + geom_col() + 
  xlab("Expl. variance (%)") + ylab("") + 
  theme(text = element_text(size=7)) + 
  theme(legend.key.size = unit(1.,"line"))
ggsave(fname, width = 1.8, height = .9)
message(sprintf("Written %s", fname))

# Add formatted t-SNE for figure
width = 2.8
height = 2.1
tmf = obj$metadata
fname = file.path(out.dir, sprintf("tsne_fg500_tanz_residency.pdf"))
ggplot(data=meta, aes(x=TSNE1, y=TSNE2, color=Label)) + 
  geom_point(show.legend = TRUE, alpha=0.4) + xlab("t-SNE 1") + ylab("t-SNE 2") + 
  ggtitle(sprintf("")) + 
  theme(text = element_text(size=7), legend.title = element_blank()) + 
  theme(legend.position="right",  legend.key.size = unit(0.6, "line")) + 
  guides(fill=guide_legend(nrow=4, override.aes = list(size = 5.5))) +
  scale_color_manual(values=c("#555555", RURAL, URBAN)) + 
  theme(text = element_text(size=7)) + 
  theme(panel.background = element_blank())
ggsave(fname, width = width, height = height)
message(sprintf("Written %s", fname))
dev.off()
fname = file.path(out.dir, sprintf("tsne_fg500_tanz_residency.csv"))
write.csv(meta, fname, row.names=F)
message(sprintf("Written %s", fname))

# Add sex
fname = file.path(out.dir, sprintf("tsne_fg500_tanz_gender.pdf"))
ggplot(data=meta[meta$Gender != "", ], 
       aes(x=TSNE1, y=TSNE2, color=Gender)) +
  geom_point(show.legend = TRUE) +
  xlab("t-SNE 1") + ylab("t-SNE 2") +
  ggtitle(sprintf("")) +
  theme(text = element_text(size=7)) +
  theme(legend.position="right",  legend.key.size = unit(0.6, "line")) +
  guides(fill=guide_legend(nrow=4, override.aes = list(size = 5.5))) +
  theme(text = element_text(size=7)) +
  theme(panel.background = element_blank()) +
  labs(color = "Sex")
ggsave(fname, width = width, height = height)
message(sprintf("Written %s", fname))
dev.off()

# Add age
fname = file.path(out.dir, sprintf("tsne_fg500_tanz_age.pdf"))
ggplot(data=meta, aes(x=TSNE1, y=TSNE2, color=Age)) +
  geom_point(show.legend = TRUE) +
  xlab("t-SNE 1") + ylab("t-SNE 2") +
  ggtitle(sprintf("")) +
  theme(text = element_text(size=7)) +
  theme(legend.position="right",  legend.key.size = unit(0.6, "line")) +
  guides(fill=guide_legend(nrow=4, override.aes = list(size = 5.5))) +
  theme(text = element_text(size=7)) +
  theme(panel.background = element_blank()) +
  labs(color = "Age")
ggsave(fname, width = width, height = height)
message(sprintf("Written %s", fname))
dev.off()

# Add QC
fname = file.path(out.dir, sprintf("tsne_fg500_tanz_markers.pdf"))
ggplot(data=meta, aes(x=TSNE1, y=TSNE2, color=log10(Markers))) + 
  geom_point(show.legend = TRUE) + 
  xlab("t-SNE 1") + ylab("t-SNE 2") + 
  ggtitle(sprintf("")) + 
  theme(text = element_text(size=7)) + 
  theme(legend.position="right",  legend.key.size = unit(0.6, "line")) + 
  guides(fill=guide_legend(nrow=4, override.aes = list(size = 5.5))) +
  theme(text = element_text(size=7)) + 
  theme(panel.background = element_blank()) +
  labs(color = "Markers (Log10)")
ggsave(fname, width = 3, height = height)
message(sprintf("Written %s", fname))
dev.off()

# Compute number of taxa in current view
meta$Taxa = rowSums(M > 0)
agg = aggregate(meta$Taxa, by=list(Label=meta$Label), median)
fname = file.path(out.dir, "cohort_number_of_taxa.pdf")
ggplot(data=meta, mapping = aes(x=reorder(Label, -Taxa), y=Taxa, fill=Label)) + 
  geom_boxplot(show.legend = F) + 
  xlab("") + ylab(sprintf("Taxa (out of %d)", ncol(M))) + 
  scale_fill_manual(values=c("#555555", RURAL, URBAN)) + 
  theme_light() + 
  theme(text=element_text(size = 7,), 
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))
ggsave(fname, width = 1.1, height = 1.8)
message(sprintf("Written %s", fname))
message("Median number of taxa:")
print(agg)

# Taxa vs. markers
fname = file.path(out.dir, "cohort_taxa_markers.pdf")
ggplot(data=meta, mapping = aes(x=Markers, y=Taxa, color=Label)) + geom_point() + 
  geom_smooth(method = "lm") + scale_x_log10() + 
  scale_color_manual(values=c("#555555", RURAL, URBAN)) + 
  theme_light() + theme(text=element_text(size = 7)) + 
  ylab(sprintf("Taxa (out of %d)", ncol(M)))
ggsave(fname, width = 3.3, height = 2.2)
message(sprintf("Written %s", fname))

# Distribution of relative abundance values
dm = melt(M)
colnames(dm) = c("Sample", "Taxon", "Abundance")
dm$Sample = as.character(dm$Sample)
dm$Taxon = as.character(dm$Taxon)
dm[,colnames(meta)] = meta[dm$Sample,]

# Abundance distribution (markers)
fname = file.path(out.dir, "cohort_abundance_distribution_markers.pdf")
ggplot(data=dm, mapping = aes(x=log10(Abundance), fill=Log10Markers)) + geom_density(alpha=.7) + 
  theme_light() + theme(text=element_text(size = 7)) + 
  ylab("Density") + xlab("Relative abundance per specie (log 10)") + 
  labs(fill = "Markers (Log10)") +
  scale_fill_manual(values = c(LOW, MID, HIGH))
ggsave(fname, width = 3.3, height = height)
message(sprintf("Written %s", fname))

# Abundance distribution (cohort)
fname = file.path(out.dir, "cohort_abundance_distribution_label.pdf")
ggplot(data=dm, mapping = aes(x=log10(Abundance), fill=Label)) + geom_density(alpha=.7) + 
  scale_fill_manual(values=c("#DDDDDD", RURAL, URBAN)) +
  theme_light() + theme(text=element_text(size = 7), 
                        legend.title = element_blank()) + 
  ylab("Density") + xlab("Relative abundance per specie (log 10)")  
ggsave(fname, width = 3.3, height = height)
message(sprintf("Written %s", fname))

# Move resultsing table from maslin
src = file.path(out.dir, "maaslin", "all_results.tsv")
dst = file.path(out.dir, "supp_table_FGS.tsv")
stopifnot(file.copy(src, dst, overwrite = T))
message(sprintf("Written %s", dst))
